//
//  ToursScrollViewController.swift
//  Busticks
//
//  Created by Levon Azbekyan on 3/20/18.
//  Copyright © 2018 Levon Azbekyan. All rights reserved.
//

import UIKit

class ToursScrollViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavbar()
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // Setup all Scroll page with selected info.
    func setupNavbar(){
        let titleLabel = UILabel()
        titleLabel.textAlignment = .center
        titleLabel.text = "Page of \(tourInfo[0])"
        titleLabel.font = UIFont(name:"Helvetica Neue", size: 25.0)
        titleLabel.textColor = UIColor.white
        self.navigationItem.titleView = titleLabel
    }
}
