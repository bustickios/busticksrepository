//
//  ToursCollectionViewController.swift
//  Busticks
//
//  Created by Levon Azbekyan on 3/11/18.
//  Copyright © 2018 Levon Azbekyan. All rights reserved.
//

import UIKit
import Foundation

// MARK: Public property
public var tourInfo = [Any]()

class ToursCollectionViewController: UICollectionViewController,UICollectionViewDelegateFlowLayout {
   
    // MARK: Property
    // Array that keep parsed data info for use in collection view cells.
    var myBusticksDestination = [MyBusticksDestination]()
    
    // Set HTTPHeaders for use in get requests.
   /* let headers: HTTPHeaders = [
        "Accept-Language" : "en",
        "Accept-Country" : "Armenia"
    ]
 */
    // Internet connection checker class.
 /*   class Connectivity {
        class func isConnectedToInternet() ->Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
 */
    // Screen size configuration property.
    // Padding on cell.
    let padding: CGFloat = 5
    // Defining hardware-based display size.
    var screenSize: CGRect = UIScreen.main.bounds
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // geting device screen width and height.
        screenWidth = screenSize.width
        screenHeight = screenSize.height
        
        // Setup Navbar logo.
        let logo = UIImage(named: "logo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        // MARK: Checking.

        // Checking internet connection.
        if Reachability.isConnectedToNetwork() == true {
            jsonDataParsing()
            self.loadingDataIndicator().startAnimating()

        } else {
            let alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
        // Checking screen size.
        if screenWidth <= 400 {
            setupLayoutDependingOnTheDevice(divisible: 2.0)
        } else {
            setupLayoutDependingOnTheDevice(divisible: 3.0)
        }

        
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return myBusticksDestination.count
    }
 
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "toursIdentifer", for: indexPath) as! ToursCollectionViewCell
        let col = self.myBusticksDestination[indexPath.row]
        
        // Converting image url to data for use.
       
 
        // Configure the cell
      /*  if indexPath.row == 0 {
            cell.frame.size = CGSize(width: screenWidth - (2 * padding), height: screenWidth/2 - padding)
            cell.imageView.frame.size = CGSize(width: screenWidth - (2 * padding), height: screenWidth/2 - padding)
        }
     */
        
       // cell.layer.borderWidth = 0.5
        if screenWidth <= 400 {
            cell.frame.size = CGSize(width: screenWidth/2 - (2 * padding), height: screenWidth/2 - padding)
            cell.imageView.frame.size = CGSize(width: screenWidth/2 - (2 * padding), height: screenWidth/2 - padding)
            

        } else {
            cell.frame.size = CGSize(width: screenWidth/3 - padding, height: screenWidth/3 - padding)
            cell.imageView.frame.size = CGSize(width: screenWidth/3 - padding, height: screenWidth/3 - padding)
        }
        
            let cellRect = cell.contentView.convert(cell.contentView.bounds, to: UIScreen.main.coordinateSpace)
                if UIScreen.main.bounds.intersects(cellRect) {
                //self.loadingDataIndicator().isHidden = true
                
        }
        
        
     
        cell.titleLabel.text! = col.translations[0].title
        cell.descriptionLabel.text! = col.translations[0].description
        
        cell.imageView.layer.cornerRadius = 5
        cell.imageView.clipsToBounds = true
        
        let url = URL(string: col.thumbnail)!
        DispatchQueue.global(qos: .userInitiated).async {
            let data =  NSData(contentsOf: url)
            DispatchQueue.main.async {
                if let dataOfTheImage = data {
                    cell.imageView.image = UIImage(data: dataOfTheImage as Data)
                    print("test")
                    
                }
            }
            
        }
        
        
        cell.layer.shouldRasterize = true
        cell.layer.rasterizationScale = UIScreen.main.scale
        
        return cell
    }
    

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    //  this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        tourInfo.removeAll()
        tourInfo += [myBusticksDestination[indexPath.row].translations[0].title,myBusticksDestination[indexPath.row].translations[0].description,myBusticksDestination[indexPath.row].slider_images]
        
        return true
    }

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */
    
    // MARK: Functions
    
    // 1 Parsing JSON.
    func jsonDataParsing(){
        let urlString = "https://www.busticks.com/api/destinations"
        
        let url = URL(string: urlString)!
        
        var request = URLRequest(url: url)
        request.setValue("en", forHTTPHeaderField: "Accept-Language")
        request.setValue("Armenia", forHTTPHeaderField: "Accept-Country")
        request.httpMethod = "GET"

        URLSession.shared.dataTask(with: request) {(data, response, error) in
            if error != nil {
                print("Parsing error.")
            } else {
                do {
                    guard let parsedData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as?
                        [Dictionary<String,AnyObject>] else {return}
                        
                        for i in 0...parsedData.count - 3 {
                            guard let sliderImages = (parsedData[i]["slider_images"]! as? [String]) else {return}
                            guard let translations = parsedData[i]["translations"]! as? [[String: String]] else {return}
                                self.myBusticksDestination.append(MyBusticksDestination(id: parsedData[i]["id"]! as! Int , thumbnail: parsedData[i]["thumbnail"]! as! String , country: parsedData[i]["country"]! as! String, slider_images: [sliderImages[0] ], translations: [TranslationsList(title: translations[0]["title"]! , description: translations[0]["description"]! , lang: translations[0]["lang"]! )] ) )
                        }
                        DispatchQueue.main.async {
                            self.collectionView?.reloadData()
                        }
                    
                } catch let error {
                    print(error)
                }
            }
        }.resume()
        
    }
    
   /* func parsingJsonWithAlamofire(url: String){
        Alamofire.request(url, method: .get, headers: headers).responseJSON { (response) in
            let array = try! JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as! [Dictionary<String,AnyObject>]
            self.loadingDataIndicator().stopAnimating()
            print("Parsing data")
            for i in 0...array.count - 2  {
                
                // Creating transletions and slider images dictonary and array.
                let tralstations = array[i]["translations"]! as! [[String: AnyObject]]
                let sliderImages = (array[i]["slider_images"]! as! [String])
                
                // Adding array items.
                self.myBusticksDestination.append(MyBusticksDestination(id: array[i]["id"]! as! Int , thumbnail: array[i]["thumbnail"]! as! String , country: array[i]["country"]! as! String, slider_images: [sliderImages[0] ], translations: [TranslationsList(title: tralstations[0]["title"]! as! String, description: tralstations[0]["description"]! as! String, lang: tralstations[0]["lang"]! as! String)] ) )
                
            }
            // Reloading collection view data that display data.
            self.collectionView?.reloadData()
            
        }
    }
    */
    // 2 Setup collection view layout.
    func setupLayoutDependingOnTheDevice(divisible: CGFloat){
       
        // Do any additional setup after loading the view, typically from a nib.
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: -5)
        layout.itemSize = CGSize(width: screenWidth/divisible, height: screenWidth/divisible)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView!.collectionViewLayout = layout
    
    }
    
    
    // Creating UIActivityIndicatorView, calling when data loading
    func loadingDataIndicator() -> UIActivityIndicatorView{
        
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 100,y:  200 ,width: 100,height:  100)) as UIActivityIndicatorView
        loadingIndicator.center = self.view.center
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        loadingIndicator.color = UIColor.red
        self.view.addSubview(loadingIndicator)
        
        return loadingIndicator
    }
  
    
    
    
    
}

