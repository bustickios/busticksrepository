//
//  ChangeLanguageViewController.swift
//  Busticks
//
//  Created by Levon Azbekyan on 3/21/18.
//  Copyright © 2018 Levon Azbekyan. All rights reserved.
//

import UIKit

class ChangeLanguageViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // Moving  to ToursCollectionViewController with btnAction.
    @IBAction func backSettingViewController(_ sender: UIButton) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let settingsTableViewController = storyBoard.instantiateViewController(withIdentifier: "toursIdentifer") as! UITabBarController
        self.present(settingsTableViewController, animated:true, completion:nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
