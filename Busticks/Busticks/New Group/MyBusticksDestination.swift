import UIKit


class MyBusticksDestination {
    let id: Int
    let thumbnail: String
    let country: String
    let slider_images: [String]
    let translations: [TranslationsList]
    
    init(id: Int, thumbnail: String, country: String, slider_images: [String], translations: [TranslationsList]) {
        self.id = id
        self.thumbnail = thumbnail
        self.country = country
        self.slider_images = slider_images
        self.translations = translations
        
    }
    
}

class TranslationsList {
    let title: String
    let description: String
    let lang: String
    
    init(title: String, description: String, lang: String) {
        self.title = title
        self.description = description
        self.lang = lang
    }
}
