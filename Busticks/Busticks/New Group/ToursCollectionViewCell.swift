//
//  ToursCollectionViewCell.swift
//  Busticks
//
//  Created by Levon Azbekyan on 3/14/18.
//  Copyright © 2018 Levon Azbekyan. All rights reserved.
//

import UIKit

class ToursCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    
}
